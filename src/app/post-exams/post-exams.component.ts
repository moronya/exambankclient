import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PostExamsService } from '../post-exams.service';
@Component({
  selector: 'app-post-exams',
  templateUrl: './post-exams.component.html',
  styleUrls: ['./post-exams.component.scss']
})
export class PostExamsComponent implements OnInit {
  paperData:any;
  shortLink: string = "";
  loading: boolean = false;
 // file: File;
  constructor(
    private formBuilder:FormBuilder,
    private postExamsService: PostExamsService
  ) {
    this.paperData = this.formBuilder.group({
      schoolName:[''],  
      yearOfStudy:[''],
      examYear:[''],
      unitCode:[''],
      unitName:[''],      
      
    //  file:['']
    })
   
   }

  ngOnInit(): void {

  }

post(): void{
console.log(this.paperData.value)
// {
//   "schoolName":"Medicine",
//   "paperDTO":{
//       "yearOfStudy":"4",
//       "unitCode":"CCS 101",
//       "examYear":2010,
//       "unitName":"Computer Graphics"
//   }
//   }
const paperDTO = { 
  yearOfStudy:this.paperData.value.yearOfStudy,
  unitCode:this.paperData.value.unitCode,
  examYear:this.paperData.value.examYear,
  unitName:this.paperData.value.unitName  
} 
const data = {  
  paperDTO: paperDTO,
  schoolName: this.paperData.value.schoolName
}
this.postExamsService.savePaper(data).subscribe(
  (res) =>{
    //this.paperData.push(res);
  },
  (err) =>
  {
    console.log(err);
  }
)
}
//handling file uploads
// onChange(event){
//   this.file = event.target.files[0];
// }

// //on file upload
// onUpload(){
//   this.loading = !this.loading;
//   console.log(this.file);
//   this.postExamsService.upload(this.file).subscribe(
//     (event:any) =>{
//       if (typeof (event) === 'object'){
//         //short link via api response
//         this.shortLink = event.link;

//         //flag the variable
//         this.loading = false;
//       }
//     }
//   )

// }
}
