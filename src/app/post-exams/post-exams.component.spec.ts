import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostExamsComponent } from './post-exams.component';

describe('PostExamsComponent', () => {
  let component: PostExamsComponent;
  let fixture: ComponentFixture<PostExamsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostExamsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PostExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
