import { TestBed } from '@angular/core/testing';

import { PostExamsService } from './post-exams.service';

describe('PostExamsService', () => {
  let service: PostExamsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PostExamsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
