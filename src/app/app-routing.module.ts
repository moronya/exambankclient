import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamsComponent } from './entities/exams/exams.component';
import { HomeComponent } from './entities/home/home.component';
import { YearComponent } from './entities/year/year.component';
import { PostExamsComponent } from './post-exams/post-exams.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path:'post-paper',
    component:PostExamsComponent
  },
  {
    path: 'schools/:name',
    component: YearComponent
  },
  {
    path: 'schools/:name/:year',
    component: ExamsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
