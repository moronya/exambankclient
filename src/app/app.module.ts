import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './entities/home/home.component';
import { NavComponent } from './layouts/nav/nav.component';
import { YearComponent } from './entities/year/year.component';
import { ExamsComponent } from './entities/exams/exams.component';
import { PostExamsComponent } from './post-exams/post-exams.component';

import {HttpClientModule} from 
    '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    YearComponent,
    ExamsComponent,
    PostExamsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
