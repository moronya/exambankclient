import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  schoolName!: string;

  filteredSchools: any;

  filtered: boolean = false;

  schools = [
    {
      name: "Computing and Informatics",
      years: [
        {
          name: "1",
          units: [
            {
              code: "CCS 101",
              name: "Fundamentals of computing"
            },
            {
              code: "CCS 102",
              name: "Linear Algebra"
            }
          ]
        }
      ]

    },
    {
      name: "Medicine",
      years: [
        {
          name: "1",
          units: [
            {
              code: "MED 101",
              name: "Anatomy"
            },
            {
              code: "MED 102",
              name: "Pathology"
            }
          ]
        }
      ]

    },
    {
      name: "Education",
      years: [
        {
          name: "1",
          units: [
            {
              code: "ED 101",
              name: "Maths"
            },
            {
              code: "ED 102",
              name: "Chemistry"
            }
          ]
        }
      ]

    }
  ]

  searchSchool() {

    if (this.schoolName.length > 0) {
      this.filtered = true;
      this.filteredSchools = this.schools.filter(school => {
        return school.name.startsWith(this.schoolName);
      });
    } else {
      this.filtered = false;
    }
    
  }

  ngOnInit(): void {
  }

}
