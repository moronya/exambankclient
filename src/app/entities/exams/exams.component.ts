import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss']
})
export class ExamsComponent implements OnInit {

  activeSchool: any;

  activeYear:any;

  constructor(
    private route:ActivatedRoute
  ) { }

  schools = [
    {
      name: "Computing and Informatics",
      years: [
        {
          name: "1",
          units: [
            {
              code: "CCS 101",
              name: "Fundamentals of computing"
            },
            {
              code: "CCS 102",
              name: "Linear Algebra"
            }
          ]
        },
        {
          name: "2",
          units: [
            {
              code: "CCS 101",
              name: "Fundamentals of computing"
            },
            {
              code: "CCS 102",
              name: "Linear Algebra"
            }
          ]
        }
      ]

    },
    {
      name: "Medicine",
      years: [
        {
          name: "1",
          units: [
            {
              code: "MED 101",
              name: "Anatomy"
            },
            {
              code: "MED 102",
              name: "Pathology"
            }
          ]
        }
      ]

    },
    {
      name: "Education",
      years: [
        {
          name: "1",
          units: [
            {
              code: "ED 101",
              name: "Maths"
            },
            {
              code: "ED 102",
              name: "Chemistry"
            }
          ]
        }
      ]

    }
  ]

  ngOnInit(): void {

    const schoolName = this.route.snapshot.paramMap.get("name")+"";

    const year = this.route.snapshot.paramMap.get("year")+"";

    this.activeSchool = this.schools.find(school=>{
      return school.name == schoolName;
    });

    this.activeYear = this.activeSchool.years.find((yearId: { name: string; })=>{
      return yearId.name = year;
    });

    console.log(this.activeYear);
    
  }

}
