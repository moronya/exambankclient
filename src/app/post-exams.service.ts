import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import {Constants} from '../app/constants';

@Injectable({
  providedIn: 'root'
})
export class PostExamsService {

  
  constructor(
    private http:HttpClient
  ) { }
 /**
   * post an exam paper
   * @param data 
   * @returns 
   */
  savePaper(data: any): Observable<any>{
    return this.http.post(Constants.API_ENDPOINT+"save-paper/", data);
  }
  //returns an Observable
  // upload(file):Observable<any>{
  //   //create form data
  //   const formData = new FormData();

  //   //storm form name as "file" with file data
  //   formData.append("file", file, file.name);

  //   //Make http post request over api
  //   //with formData as request
  //   return this.http.post(this.baseApiUrl, formData);
  // }
}
